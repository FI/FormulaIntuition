#include <iostream>
#include <vector>

using namespace std;

void output(vector<vector<int> > v, int N) {
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            cout << v[i][j] << "\t";
        }
        cout << endl;
    }
}

//========================================================================================================================
//
//                               ===============================
//                                        Backtracking
//                                         Algorithms
//                               ===============================
//
//                                           Q&A!
//                  ==========================================================
//                                        What is it?
//
//                      Backtracking solves constraint satisfaction problems.
//                      Which involves exhaustively searching through
//                      every contraint until we find a solution.
//
//                      Backtracking does not search for the optimal
//                      solution(dynamic programming), instead it looks
//                      for ALL solutions or ANY solution.
//
//                      Backtracking is identical to depth-first search.
//                      We simply use a different name depending on the
//                      problem we're solving.
//                  ==========================================================
//
//
//                           First Problem: Knights Tour
//
//                           Description: Move a knight through every
//                                        position on a board without
//                                        visiting the same position
//                                        twice
//
//                                        Note: There is only ONE solution
//
//                           Examples output:
//                                        0	 59	38	33	30	17	8	63	
//                                        37 34	31	60	9	62	29	16	
//                                        58 1	36	39	32	27	18	7	
//                                        35 48	41	26	61	10	15	28	
//                                        42 57	2	49	40	23	6	19	
//                                        47 50	45	54	25	20	11	14	
//                                        56 43	52	3	22	13	24	5	
//                                        51 46	55	44	53	4	21	12
//
//
//========================================================================================================================









bool canMoveKT(int c, int r, vector<vector<int> > &a, int N) {
    return c >= 0 && c < N && r >= 0 && r < N && a[c][r] == 0;
}

bool knightstour(int c, int r, int index, vector<vector<int> > &a, int N, int n) {
    if (!canMoveKT(c, r, a, N)) return false;

    a[c][r] = n++;
    if (index == N*N-1) return true;
    if (knightstour(c+2, r+1, index+1, a, N, n))
        return true;
    if (knightstour(c+1, r+2, index+1, a, N, n))
        return true;
    if (knightstour(c-1, r+2, index+1, a, N, n))
        return true;
    if (knightstour(c-2, r+1, index+1, a, N, n))
        return true;
    if (knightstour(c-2, r-1, index+1, a, N, n))
        return true;
    if (knightstour(c-1, r-2, index+1, a, N, n))
        return true;
    if (knightstour(c+1, r-2, index+1, a, N, n))
        return true;
    if (knightstour(c+2, r-1, index+1, a, N, n))
        return true;

    a[c][r] = 0; // backtrack
    n--;
    return false;
}








//========================================================================================================================
//
//
//                           Second Problem: Maze Solving
//
//                           Description: Find a path from the start
//                                        to end position in a maze
//
//
//========================================================================================================================












bool canMove(vector<vector<int> > &maze, vector<vector<int> > &solution, int c, int r, int N) {
    return (c >= 0 && r >= 0 && c < N && r < N &&
            maze[c][r] == 1 && solution[c][r] == 0);
}

bool findPath(vector<vector<int> > &maze, vector<vector<int> > &solution, int c, int r, int N, int n) {
    if (!canMove(maze, solution, c, r, N)) return false;

    solution[c][r] = n++;
    if (c == N-1 && r == N-1) return true;
    if (findPath(maze, solution, c, r+1, N, n)) // right
        return true;
    if (findPath(maze, solution, c+1, r, N, n)) // down
        return true;
    if (findPath(maze, solution, c, r-1, N, n)) // left
        return true;
    if (findPath(maze, solution, c-1, r, N, n)) // up
        return true;

    solution[c][r] = 0; // backtrack
    n--;
    return false;
}


















//========================================================================================================================
//
//
//                           Third Problem: N Queens
//
//                           Description: Find the TOTAL number of ways
//                                        you can place 'n' queens on a
//                                        n*n chessboard. 
//
//                           Example: n = '8' = 92
//                                                    8 |q|#|_|#|_|#|_|#|
//                                                    7 |#|_|q|_|#|_|#|_|
//                                                    6 |_|#|_|#|q|#|_|#|
//                                                    5 |#|q|#|_|#|_|#|_|
//                                                    4 |_|#|_|#|_|#|_|q|
//                                                    3 |#|q|#|_|#|_|#|_|
//                                                    2 |_|#|_|#|_|#|_|#|
//                                                    1 |#|_|#|_|#|_|#|_|
//                                                       a b c d e f g h
//
//
//========================================================================================================================









bool validNQ(int row, int col, vector<vector<int> > &board, int N) {
    // checking same column
    for (int i = 0; i < N; i++)
        if (board[i][col] == 1)
            return false; // checking negative slope
    for (int i = row, j = col; i >= 0 && j >= 0; i--, j--)
        if (board[i][j] == 1)
            return false;
    // checking positive slope
    for (int i = row, j = col; i >= 0 && j < N; i--, j++)
        if (board[i][j] == 1)
            return false;
    return true;
}


void NQ(int row, int &n, vector<vector<int> > &board, int N) {
    if (row == N) {
        n++;
        return;
    }

    for (int i = 0; i < N; i++) {
        if (validNQ(row, i, board, N)) {
            board[row][i] = 1;
            NQ(row+1, n, board, N);
            board[row][i] = 0; // backtrack
        }
    }
}












// {2, 4 ,6 ,8}, sum = 8 {2, 2, 2, 2}
void combinationsum(vector<int>& arr, int sum, int index, vector<int> &r, vector<vector<int> > &res) {
    if (sum == 0) {
        res.push_back(r);
        return;
    }
 
    while (index < arr.size() && sum - arr[index] >= 0) {
 
        r.push_back(arr[index]); // add them to list
        combinationsum(arr, sum - arr[index], index, r, res);
        r.pop_back(); // backtrack
        index++;
    }
}


















int main() {
    int N = 8;
    int n = 0;

    vector<vector<int> > a(N, vector<int>(N));
    if (knightstour(0, 0, 0, a, N, n)) {
        cout << "solved" << endl;
        output(a, N);
    }
    else
        cout << "not solved" << endl;

    vector<vector<int> > maze { { 1, 0, 1, 1, 1 },
                                { 1, 1, 1, 0, 1 },
                                { 0, 0, 0, 1, 1 },
                                { 0, 0, 0, 1, 0 },
                                { 0, 0, 0, 1, 1 } };

    N = 5;
    n = 1;
    vector<vector<int> > solution(N, vector<int>(N));
    if (findPath(maze, solution, 0, 0, N, n)) {
        output(solution, N);
    } else {
        cout << "solution not found" << endl;
    }

    N = 8;
    n = 0;
    vector<vector<int> > board(N, vector<int>(N));
    NQ(0, n, board, N);
    cout << n << endl;

    /*
    vector<int> ar{2,4,6,8};
    int n = ar.size();
    int sum = 8;
    vector<vector<int> > res = combinationSum(ar, sum);

    // remove duplicates
    sort(ar.begin(), ar.end());
    ar.erase(unique(ar.begin(), ar.end()), ar.end());
 
    vector<int> r;
    vector<vector<int> > res;
    combinationsum(ar, sum, res, r, 0);
     */
}
