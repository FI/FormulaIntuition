#include <iostream>
#include <vector>

using namespace std;

//========================================================================================================================
//
//                               ===============================
//                                            XOR
//                                         Algorithms
//                               ===============================
//
//                                           Q&A!
//                  ==========================================================
//                                        What is it?
//
//                      An 'xor' is a logical operation that operates on
//                      binary numbers. In c++, we use the '^' symbol to
//                      xor two values.
//
//                                      0 xor 0 == 0
//                                      0 xor 1 == 1
//                                      1 xor 0 == 1
//                                      1 xor 1 == 0
//
//                      From this we can observe the rule of xor:
//                       - xor outputs '0' when the inputs are the SAME.
//                       - xor outputs '1' when the inputs are DIFFERENT.
//
//                      We can apply xor on values greater than one bit
//                      In fact, we can do it to any value that is in
//                      binary. This is called 'bitwise' xor, meaning
//                      we apply an xor operation on each individual
//                      bit:
//
//                                    101101 xor 010110:
//
//                                      |1|1|1|1|0|1|
//                                      |0|1|0|1|1|0| xor
//                                      | | | | | | |
//                                    = |1|0|1|0|1|1|
//
//                      The most obvious use case of xor is that it lets
//                      us cancel out duplicate numbers very easily. If
//                      the same number appears twice, xor'ing them
//                      returns zero.
//
//                  ==========================================================
//
//
//                           First Problem: Single Number
//
//                           Description: Only one number in the array
//                                        is not a duplicate, find the
//                                        number in O(1) space
//
//                           Example: input: [1, 1, 1, 1, 3, 2, 2]
//                                    output: [3]
//
//                           method: xor every number in the array then
//                                   return the final number
//
//
//========================================================================================================================

int singleNumber(vector<int> &nums) {
}

//========================================================================================================================
//
//
//                           Second Problem: Two Single Numbers
//
//                           Description: Only two numbers in the array
//                                        are not duplicates, find them
//                                        both in O(1) space
//
//                           Example: input: [1, 1, 2, 2, 3, 4]
//                                    output: [3, 4]
//
//                           method: Same method as above except we are
//                                   left with the xor result of two
//                                   numbers. Isolate the numbers from
//                                   the result then return them.
//
//
//========================================================================================================================

vector<int> singleNumber2(vector<int> &nums) {
}

//========================================================================================================================
//
//
//                           Extra Problem: Number of bits
//
//                           Description: Determine the number of bits
//                                        you need to flip to convert
//                                        integer a to integer b
//
//
//                           example: input: a: 11111, b: 01110
//                                    output: 2
//
//                           method: xor both numbers to see where
//                                   the bits differ, count each
//                                   '1' bit.
//
//
//========================================================================================================================

int numberofbits(int a, int b) {
}

void output(vector<int> a) {
    for (auto it = a.begin(); it != a.end(); ++it) {
        cout << *it << " ";
    }
    cout << endl;
}

int main() {
    vector<int> a{1, 1, 3, 3, 2, 4, 4};
    vector<int> b{1, 1, 3, 3, 4, 5};
    cout << singleNumber(a) << endl;
    output(singleNumber2(b));
    cout << numberofbits(2, 6) << endl;
}
