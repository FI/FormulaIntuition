#include <iostream>

using namespace std;

struct Node {
    int val;
    struct Node *next;

    Node(int val) {
        this->val = val;
        this->next = nullptr;
    }
};

//========================================================================================================================
//
//                               ===============================
//                                    Fast & Slow Pointers!          
//                                AKA Floyd's Tortoise and Hare  
//                               ===============================
//
//                                           Q&A!
//                  ==========================================================
//                                        What is it?
//
//                      This is a classical algorithm that solves cycle
//                      detection problems. It uses the idea of a two pointer
//                      traversal. We have a special name for these two
//                      pointers, tortoise & hare. This is because the
//                      tortoise is the slow pointer and the hare is the
//                      fast pointer.
//
//                      The fast pointer(hare) traverses twice as fast as the
//                      slow pointer(tortoise). So if slow has moved a
//                      distance 'd', then fast has moved a distance '2d'
//                  ==========================================================
// 
//
//                           First Problem: Middle of a Linked List
//
//                           Description: find the middle of a singly
//                                        linked-list.
//
//                           Brute force: O(N + N/2) 
//                           Floyd's    : O(N)
//
//                           Method: traverse the fast and slow pointers
//                                   until fast reaches the end.
//
//                                   since the hare moves twice as fast,
//                                   the tortoise should be at the MIDDLE
//                                   of the list when the hare reaches the
//                                   end.
//
//========================================================================================================================

int middleOfList(struct Node* head) {
    struct Node *tort = head;
    struct Node *hare = head;

    while (hare != nullptr && hare->next != nullptr) {
        tort = tort->next;
        hare = hare->next->next;
    }

    return tort->val;
}

//========================================================================================================================
// 
//
//                                Second Problem: Duplicate Number
//
//                           Description: Find the duplicate number in
//                                        an array 
//
//                           Constraint: - Size is N+1 (so we can index N)
//                                       - Contains numbers from 1 to N
//                                         with one repeated number
//                                       - Space of O(1)
//
//                       -----------------------------------------------------
//                                      Pigeonhole Principle
//
//                           e.g. [1, 2, 3, 4, 5, 3] => Length: 6 
//
//                                We are filling in 6 slots with numbers
//                                from 1 to 5
//
//                                when we are here:
//                                [1, 2, 3, 4, 5, ?] => length: 5
//                                
//                                we have one more slot to fill in,
//                                notice how there has to be a repeated
//                                number in order to fill in that slot.
//                                Hence the above example.
//
//
//                  =============================================================
//
//
//                                      Part 1: Cycle Detection
//
//                           Imagine a race track with two cars A & B
//                           B is constantly twice as fast as Car A. So B
//                           is always going to be ahead of car A.
//                           Eventually B will lap around the race track and 
//                           meet up with car A again. That's what we call a
//                           'cycle'.
//                       -----------------------------------------------------
//
//                                      Part 2: Finding the Duplicate
//                           Suppose the slow pointer has moved a distance 'd',
//                           then the fast pointer has moved a distance '2d'.
//
//                           The distance towards the loop is 'a'
//                           The distance around the loop is 'b + c'
//                           where b is the distance from SOL to the collision
//                           and c is distance from the collision to the SOL;
//
//                           slow pointer covers a & b
//                           fast pointer covers a & b & c and b again
//
//                       -----------------------------------------------------
//
//                       This means that the distance from the beginning of
//                       the list to the start of the loop, and the distance
//                       from the collision point to the start of the loop
//                       are EQUAL. So if we set two pointers at both of
//                       those positions and traverse them both at the SAME
//                       speed, they will meet up at the start of the loop,
//                       which gives us the repeated number.
//
//                  ==============================================================
//
//========================================================================================================================

int findDuplicate(int a[]) {
    int tort = a[0];
    int hare = a[0];
    
    while (true) {
        tort = a[tort];
        hare = a[a[hare]];
        if (tort == hare) {
            // return true;
            break;
        }
    }
    int l = a[0];
    int r = tort;
    while (l != r) {
        l = a[l];
        r = a[r];
    }
    return l;
}

int findDuplicate(struct Node *head) {
}

//========================================================================================================================
// 
//
//                                    Third Problem: Happy Numbers
//
//                           Description: A happy number is a number that
//                                        converges to 1 when the following
//                                        computation is repeated:
//
//                                        Calculate the sum of squares of
//                                        the digits of an integer. 
//
//                                        e.g. 129 = 1^2 + 2^2 + 9^3
//                                                 = 1   + 4   + 81
//                                                 = 86
//                                              86 = 8^2 + 6^2
//                                                 = 64  + 36
//                                                 = 100
//                                             100 = 1^2 + 0^2 + 0^2
//                                                 = 1
//                                        Therefore, 129 is a happy number
//
//
//                       -----------------------------------------------------
//                                      Lets look at an example
//                                      of a number that isn't happy
//
//                                            12 = 1^2 + 2^2
//                                               = 1   + 4 = 5
//                                             5 = 5^2
//                                               = 25
//                                            25 = 2^2 + 5^2
//                                               = 4   + 25 = 29
//                                            29 = 2^2 + 9^2
//                                               = 4   + 81 = 85
//                                            85 = 8^2 + 5^2
//                                               = 64  + 25 = 89
//                              SOL  |----->  89 = 8^2 + 9^2
//                                   |           = 64 + 81 = 145
//                                   |       145 = 1^2 + 4^2 + 5^2
//                                   |           = 1   + 16  + 25 = 42
//                                   |        42 = 4^2 + 2^2
//                                   |           = 16 + 4 = 20
//                                   |        20 = 2^2 + 0^2
//                                   |           = 4
//                                   |         4 = 4^2
//                                   |           = 16
//                                   |        16 = 1^2 + 6^2
//                                   |           = 1 + 36 = 37
//                                   |        37 = 3^2 + 7^2
//                                   |           = 9   + 49 = 58
//                                   |        58 = 5^2 + 8^2
//                                   |           = 25  + 64 = 89
//                                   |----->  89 = ...
//
//
//                  =============================================================
//
//========================================================================================================================

bool happyNumber() {
    return 1;
}

int main() {
    int a[] = {1, 2, 3, 4, 5, 6, 7, 5, 8};
    int b[] = {3, 1, 3, 4, 2};

    struct Node *head;
    head = new Node(1);
    head->next = new Node(2);
    head->next->next = new Node(3);
    head->next->next->next = new Node(4);
    head->next->next->next->next = new Node(5);
    head->next->next->next->next->next = new Node(6);
    head->next->next->next->next->next->next = new Node(7);
    head->next->next->next->next->next->next->next = new Node(5);
    head->next->next->next->next->next->next->next->next = new Node(8);

    cout << middleOfList(head) << endl;

    struct Node *T = head;
    while (T != nullptr) {
        struct Node *t = T->next;
        free(T);
        T = t;
    }
    cout << "deallocated" << endl;
}
